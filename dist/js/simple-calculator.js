"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Calculator =
function () {
  function Calculator(buttonClass, previousNumberScreen, currentNumberScreen) {
    _classCallCheck(this, Calculator);

    this.buttonClass = buttonClass;
    this.previousNumberScreen = previousNumberScreen;
    this.currentNumberScreen = currentNumberScreen;
    this.previousNumber = '';
    this.currentNumber = '';
    this.currentOperation = undefined;
    this.equalsIsActive = false;
  } 


  _createClass(Calculator, [{
    key: "ac",
    value: function ac() {
      this.previousNumber = '';
      this.currentNumber = '';
      this.currentOperation = undefined;
      this.updateScreen();
    } 

  }, {
    key: "del",
    value: function del() {
      this.currentNumber = this.currentNumber.toString().slice(0, -1);
      this.updateScreen();
    } 

  }, {
    key: "appendNumber",
    value: function appendNumber(number) {
      if (number === '.' && this.currentNumber.includes('.')) return;
      this.currentNumber = this.currentNumber.toString() + number.toString();
      this.updateScreen();
    } 

  }, {
    key: "updateScreen",
    value: function updateScreen() {
      document.querySelector(this.currentNumberScreen).innerText = this.currentNumber;

      if (this.currentOperation != null) {
        document.querySelector(this.previousNumberScreen).innerHTML = this.previousNumber + '<span>' + this.currentOperation + '</span>';
      } else if (this.previousNumber == '' && this.currentNumber == '') {
        document.querySelector(this.previousNumberScreen).innerHTML = '';
      }
    } 

  }, {
    key: "setOperation",
    value: function setOperation(operation) {
      if (this.currentNumber === '') return; 

      if (this.previousNumber != '') {
        this.calculate();
      }

      this.currentOperation = operation;
      this.previousNumber = this.currentNumber;
      this.currentNumber = '';
      this.updateScreen();
    } 

  }, {
    key: "setEquals",
    value: function setEquals() {
      if (this.equalsIsActive === true) return;
      this.equalsIsActive = true;
    } 

  }, {
    key: "calculate",
    value: function calculate() {
      var result; 

      var previous = parseFloat(this.previousNumber);
      var current = parseFloat(this.currentNumber); 

      if (isNaN(previous) || isNaN(current)) return; 

      switch (this.currentOperation) {
        case '+':
          result = previous + current;
          break;

        case '-':
          result = previous - current;
          break;

        case '÷':
          result = previous / current;
          break;

        case '×':
          result = previous * current;
          break;

        default:
          return;
      }

      this.currentNumber = result;
      this.currentOperation = undefined;
      this.previousNumber = '';
      this.updateScreen();
    } 

  }, {
    key: "handleClicks",
    value: function handleClicks() {
      var _this = this;

      var buttons = document.querySelectorAll(this.buttonClass);
      buttons.forEach(function (button) {
        var inputType = button.dataset.inputType;

        switch (inputType) {
          case 'number':
            button.addEventListener('click', function () {
              if (_this.equalsIsActive === false) {
                _this.appendNumber(button.innerText);
              } 
              else {
                  _this.ac();

                  _this.appendNumber(button.innerText);

                  _this.equalsIsActive = false;
                }
            });
            break;

          case 'operation':
            button.addEventListener('click', function () {
              if (_this.equalsIsActive === false) {
                _this.setOperation(button.innerText);
              } else {
                _this.ac();

                _this.setOperation(button.innerText);

                _this.equalsIsActive = false;
              }
            });
            break;

          case 'ac':
            button.addEventListener('click', function () {
              _this.ac();
            });
            break;

          case 'del':
            button.addEventListener('click', function () {
              _this.del();
            });
            break;

          case 'equals':
            button.addEventListener('click', function () {
              _this.calculate();

              _this.setEquals();
            });
            break;

          default:
            return;
        }
      });
    } 

  }, {
    key: "init",
    value: function init() {
      this.ac();
      this.handleClicks();
    }
  }]);

  return Calculator;
}();