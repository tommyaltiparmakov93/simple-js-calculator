const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const jshint = require('gulp-jshint');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const strip = require('gulp-strip-comments');

const src = {
  js: 	'js/',
  scss: 'scss/**/*.scss'
}

const dist = {
  js: 	'../dist/js/',
  css:  '../dist/css/'
}

function styles() {
  return gulp.src(src.scss)
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postcss([autoprefixer()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist.css));
}

function scripts() {
   return gulp.src([
    src.js + 'libs/*.js',
    src.js + '**/*.js'
   ])
      .pipe(jshint())
      .pipe(babel({
            presets: ['@babel/env']
      }))
      .pipe(concat('simple-calculator.js'))
      .pipe(strip())
      .pipe(gulp.dest(dist.js));
}

function watchFiles() {
    gulp.watch(src.scss, styles);
    gulp.watch(src.js + '**/*.js', scripts);
}

// Tasks
gulp.task('styles', styles);
gulp.task('scripts', scripts);

// watch
gulp.task('watch', gulp.parallel(watchFiles));

// Default Task
gulp.task('default', gulp.parallel('watch'));
