class Calculator {

  constructor(buttonClass, previousNumberScreen, currentNumberScreen) {
    this.buttonClass = buttonClass;
    this.previousNumberScreen = previousNumberScreen;
    this.currentNumberScreen = currentNumberScreen;
    this.previousNumber = '';
    this.currentNumber = '';
    this.currentOperation = undefined;
    this.equalsIsActive = false;
  }

  // Clear operations and set them to default
  ac() {
    this.previousNumber = '';
    this.currentNumber = '';
    this.currentOperation = undefined;
    this.updateScreen();
  }

  // Removing single item off the end of an operation
  del() {
    this.currentNumber = this.currentNumber.toString().slice(0, -1);
    this.updateScreen();
  }

  // Stick digits together to form one number
  appendNumber(number) {
    // if period exist then stop from adding more
    if( number === '.' && this.currentNumber.includes( '.' ) ) return;
    this.currentNumber = this.currentNumber.toString() + number.toString();
    this.updateScreen();
  }

  // Pushing data to the screen of the calculator
  updateScreen() {
    document.querySelector(this.currentNumberScreen).innerText = this.currentNumber;

    if( this.currentOperation != null ) {
      document.querySelector(this.previousNumberScreen).innerHTML = this.previousNumber + '<span>'+ this.currentOperation +'</span>';
    } else if ( this.previousNumber == '' && this.currentNumber == '' ) {
      document.querySelector(this.previousNumberScreen).innerHTML = '';
    }
  }

  // This is run whenever a user chooses an operation ie. +, -, / or *
  // Setting the operation type
  // Handling calculations
  // Setting previous operation to equal to current operation
  setOperation(operation) {
    // Stop users from choosing an operation if there are no numbers to calculate
    if( this.currentNumber === '' ) return;

    // If there is no data inside previous operation, we want to calculate previous with current operation
    if( this.previousNumber != '' ) {
      this.calculate();
    }

    this.currentOperation = operation;
    this.previousNumber = this.currentNumber;
    this.currentNumber = '';
    this.updateScreen();
  }

  // Define whether or not the user has clicked equals
  setEquals() {
    if( this.equalsIsActive === true ) return;
    this.equalsIsActive = true;
  }

  // Called when equals is clicked
  // Define operation types
  // Calculate the inputs
  // Set current operation to result
  calculate() {
    let result;
    // Convert strings to numbers that can be used for calculations
    const previous = parseFloat( this.previousNumber );
    const current = parseFloat( this.currentNumber );
    // Stop code block from running if the user clicks equals when there is no previous or current operation
    if( isNaN( previous ) || isNaN( current ) ) return;

    // Define each type of calculation
    switch(this.currentOperation) {
      case '+':
        result = previous + current;
        break;

      case '-':
        result = previous - current;
        break;

      case '÷':
        result = previous / current;
        break;

      case '×':
        result = previous * current;
        break;

      default:
        return;
    }

    this.currentNumber = result;
    this.currentOperation = undefined;
    this.previousNumber = '';
    this.updateScreen();
  }

  // Define all click events
  handleClicks() {
    let _this = this;
    let buttons = document.querySelectorAll(this.buttonClass);

    buttons.forEach(( button ) => {
       let inputType = button.dataset.inputType;

       switch(inputType) {
         case 'number':
           button.addEventListener('click', () => {
             // If equals has not been clicked, perform normal operations
             if( _this.equalsIsActive === false ) {
               _this.appendNumber(button.innerText);
             }
             // If equals has been clicked then after clicking a button reset to default
             else {
               _this.ac();
               _this.appendNumber(button.innerText);
               _this.equalsIsActive = false;
             }
           });
           break;

         case 'operation':
           button.addEventListener('click', () => {
             if( _this.equalsIsActive === false ) {
               _this.setOperation(button.innerText);
             } else {
               _this.ac();
               _this.setOperation(button.innerText);
               _this.equalsIsActive = false;
             }
           });
           break;

         case 'ac':
            button.addEventListener('click', () => {
             _this.ac();
           });
           break;

         case 'del':
           button.addEventListener('click', () => {
             _this.del();
           });
           break;

         case 'equals':
           button.addEventListener('click', () => {
             _this.calculate();
             _this.setEquals();
           });
           break;

         default:
           return;
       }
    });
  }

  // Run methods on object instantiation
  init() {
    this.ac();
    this.handleClicks();
  }
}
