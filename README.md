# Simple JS Calculator

This is a simple Javascript Class that creates a calculator. Feel free to add this to your own project and have a play with it! The goal was to make it quick and easy to get up and running.

## Usage

If you want to get this up and running quickly then simply define a common class for the calculator button ie. calc__btn and then the class for the previous and current operations screen elements and then instantiate an object of the Calculator class. Eg. below:

```javascript
let calc = new Calculator('.calc__btn', '.previous-number', '.current-number');
calc.init();
```
And of course include the simple-calculator.js file in your project from the dist/ directory.

One thing you will notice in the HTML, is that there is a use of data attributes. These help define the kind of input that each button is handling. If the button is responsible for a plus or minus then the data attribute should be data-input-type="operation" for example.

## Installation

If you're looking to dive into the source code and have a play then use the package manager [npm](https://www.npmjs.com/) to install the required dependancies.

```bash
1. cd into the src/ directory
2. npm install
3. run gulp watch to begin compiling from src/ to dist/
```
Babel is used to convert source Javascript into browser ready Javascript.

## Demo

You can see the dist version here: https://codepen.io/TomAlti93/pen/abzdZpo

## Last but not least

Have fun! Break it, gut it out, re-build or make it better. It's all up to you!
